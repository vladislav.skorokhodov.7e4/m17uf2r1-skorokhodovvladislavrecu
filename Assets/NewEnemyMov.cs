using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NewEnemyMov : MonoBehaviour
{
    // Start is called before the first frame update
    private NavMeshAgent agent;
    private bool targetIsPlayer;
    private Animator animator;
    private Transform playerPos;

    public Transform[] positions;
    private int actualPos = 0;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //ChangeTarget();
        if (!gameObject.GetComponent<FieldOfView>().canSeePlayer)
        {
            goToPosition();
        }
        
    }

    private void LateUpdate()
    {
        AnimationChanges();

        if (gameObject.GetComponent<FieldOfView>().canSeePlayer)
        {
            agent.destination = playerPos.position;
        }
    }

    private void AnimationChanges()
    {
        Debug.Log(agent.remainingDistance);
        animator.SetFloat("State", agent.velocity.magnitude);

        if (agent.remainingDistance <= 1.5f && targetIsPlayer)
        {
            animator.SetTrigger("Attack");
            agent.isStopped = true;
        }
    }

    public void resetStopAgent()
    {
        agent.isStopped = false;
    }

    private void goToPosition()
    {
        if(agent.remainingDistance <= 0.5)
        {
            actualPos++;
            if(actualPos >= positions.Length)
            {
                actualPos = 0;
            }
            agent.destination = positions[actualPos].position;
        }
    }
}
