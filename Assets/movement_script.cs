using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement_script : MonoBehaviour
{


    CharacterController Controller;

    public float Speed;
    private float run_multiplicator;

    public Transform Cam;

    private Animator animator;
    
    
    Rigidbody m_Rigidbody;
    bool m_IsGrounded;
    Animator m_Animator;
    public float m_JumpPower = 5;


    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        Controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {
        movement();
        
    }

    void movement()
    {

        if (Input.GetAxis("Vertical") >0)
        {
            animator.SetBool("walking_forward", true);
            animator.SetBool("walking_back", false);
            if (Input.GetKey("left shift")){
                animator.SetBool("run", true);
                run_multiplicator = 2;
            }
            else if (Input.GetKey("left ctrl"))
            {
                animator.SetBool("crouch", true);
                run_multiplicator = 0.5f;
            }
            else
            {
                animator.SetBool("run", false);
                animator.SetBool("crouch", false);
                run_multiplicator = 1;
            }
        }
        else if(Input.GetAxis("Vertical") < 0)
        {
            animator.SetBool("walking_forward", false);
            animator.SetBool("walking_back", true);
            if (Input.GetKey("left ctrl"))
            {
                animator.SetBool("crouch", true);
                run_multiplicator = 0.5f;
            }
            else
            {
                animator.SetBool("run", false);
                run_multiplicator = 1;
            }
        }
        else if(Input.GetAxis("Vertical") == 0)
        {
            animator.SetBool("walking_forward", false);
            animator.SetBool("walking_back", false);
            if (Input.GetKey("left ctrl"))
            {
                animator.SetBool("crouch", true);
                run_multiplicator = 0.5f;
            }
            else
            {
                animator.SetBool("crouch", false);
                run_multiplicator = 1;
            }
        }

        float Horizontal = Input.GetAxis("Horizontal") * Speed * Time.deltaTime;
        float Vertical = Input.GetAxis("Vertical") * Speed * Time.deltaTime;

        Vector3 Movement = Cam.transform.right * Horizontal + Cam.transform.forward * Vertical * run_multiplicator;
        Movement.y = 0f;



        Controller.Move(Movement);

        if (Movement.magnitude != 0f)
        {
            transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * Cam.GetComponent<CameraMove>().sensivity * Time.deltaTime);


            Quaternion CamRotation = Cam.rotation;
            CamRotation.x = 0f;
            CamRotation.z = 0f;

            transform.rotation = Quaternion.Lerp(transform.rotation, CamRotation, 0.1f);

        }

        
    }


    void HandleGroundedMovement(bool crouch, bool jump)
    {
        // check whether conditions are right to allow a jump:
        if (jump && !crouch && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Grounded"))
        {
            // jump!
            m_Rigidbody.velocity = new Vector3(m_Rigidbody.velocity.x, m_JumpPower, m_Rigidbody.velocity.z);
            m_IsGrounded = false;
            m_Animator.applyRootMotion = false;
            //m_GroundCheckDistance = 0.1f;
        }
    }

}
