using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform initialGoal;

    private NavMeshAgent agent;
    private bool targetIsPlayer;
    private Animator animator;
    private Transform playerPos;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.destination = initialGoal.position;
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        ChangeTarget();
        
    }

    private void LateUpdate()
    {
        AnimationChanges();

        if (targetIsPlayer)
        {
            agent.destination = playerPos.position;
        }
    }

    private void ChangeTarget()
    {
        if (Input.GetKeyDown(KeyCode.Return) && targetIsPlayer)
        {
            agent.destination = playerPos.position;
            targetIsPlayer = false;
        }
        else if (Input.GetKeyDown(KeyCode.Return) && !targetIsPlayer)
        {
            agent.destination = initialGoal.position;
            targetIsPlayer = true;
        }
        
    }

    private void AnimationChanges()
    {
        Debug.Log(agent.remainingDistance);
        animator.SetFloat("State", agent.velocity.magnitude);

        if (agent.remainingDistance <= 1.5f && targetIsPlayer)
        {
            animator.SetTrigger("Attack");
            agent.isStopped = true;
        }
    }

    public void resetStopAgent()
    {
        agent.isStopped = false;
    }
}
