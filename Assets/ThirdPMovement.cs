using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPMovement : MonoBehaviour
{
    public CharacterController characterController;
    public Transform cam;

    public float speed = 3f;

    public float smoothTime = 0.1f;
    float turnSmoothVelocity;
    private Animator animator;
    private float run_multiplicator;
    //private bool isGrounded = true;
    private Rigidbody RB;
    public float jumpForce = 10;
    private Vector3 moveDirection = Vector3.zero;
    public float gravity = 10f;
    private int stopped= 1; 

    private void Start()
    {
        
        RB = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();    }

    void Update()
    {
        float hori = Input.GetAxisRaw("Horizontal");
        float vert = Input.GetAxisRaw("Vertical");
        Vector3 direction = new Vector3(hori, 0, vert).normalized;

        if (direction.magnitude >= 0.1f)
        {
            float targrtAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targrtAngle, ref turnSmoothVelocity, smoothTime);
            transform.rotation = Quaternion.Euler(0, angle, 0);


            Vector3 moveDir = Quaternion.Euler(0f, targrtAngle, 0f) * Vector3.forward;
            characterController.Move(moveDir.normalized * speed * Time.deltaTime * run_multiplicator * stopped);


            
        }

        if (characterController.isGrounded)
        {
            animator.SetBool("on_air", false);
            if (Input.GetButton("Jump"))
            {
                moveDirection.y = jumpForce;
                animator.SetBool("JUMP", true);
                animator.SetBool("on_air", true);
            }
            animator.SetBool("JUMP", false);

        }

       
        moveDirection.y -= gravity * Time.deltaTime;
        characterController.Move(moveDirection * Time.deltaTime);

        movement_anim();
        //Debug.Log(isGrounded);
        //jump();

        if (Input.GetKey(KeyCode.E))
        {
            stopped = 0;
            animator.SetBool("pick", true);
        }

        if (Input.GetKey(KeyCode.Q))
        {
            stopped = 0;
            animator.SetBool("good", true);
        }

        //Debug.Log(animator.GetFloat("Speed"));
    }

    void movement_anim()
    {

        if (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") !=0)
        {
            //Debug.Log("W");
            animator.SetBool("walking_forward", true);
            animator.SetBool("walking_back", false);
            animator.SetFloat("Speed", 0.5f);
            if (Input.GetKey("left shift"))
            {
                //Debug.Log("W Shift");
                animator.SetBool("run", true);
                run_multiplicator = 2;
                animator.SetFloat("Speed", 1);
            }
            else if (Input.GetKey("left ctrl"))
            {

                animator.SetBool("crouch", true);
                run_multiplicator = 0.5f;
                animator.SetFloat("Speed", 1);

            }
            else
            {
                animator.SetBool("run", false);
                animator.SetBool("crouch", false);
                run_multiplicator = 1;
            }
        }
        
        else if (Input.GetAxis("Vertical") == 0)
        {
            animator.SetFloat("Speed", 0);
            animator.SetBool("walking_forward", false);
            animator.SetBool("walking_back", false);
            if (Input.GetKey("left ctrl"))
            {
                animator.SetBool("crouch", true);
                run_multiplicator = 0.5f;
            }
            else
            {
                animator.SetBool("crouch", false);
                run_multiplicator = 1;
            }
        }


    }

    /*public void jump()
    {
        if (characterController.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpForce;
        }
        moveDirection.y -= gravity * Time.deltaTime;
        characterController.Move(moveDirection * Time.deltaTime);
    }*/

    public void stop_stopping()
    {
        stopped = 1;
        animator.SetBool("pick", false);
        animator.SetBool("good", false);
    }
}

